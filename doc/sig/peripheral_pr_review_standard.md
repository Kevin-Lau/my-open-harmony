请检查提交PR是否满足[《HDF代码上库规范要求》](https://gitee.com/openharmony/drivers_hdf_core/wikis/specification/sig_driver_issue_pr_commit_review_standard), 否则将不会审查通过。

|自查项|示例|
|:---|:---|
|是否触发门禁构建？|评论命令"**start build**"触发门禁构建|
|是否门禁静态检查pass？|触发门禁构建大约60分钟后，查看构建结果和构建门禁静态检查结果，是否全部**pass**，未**pass**需要解决问题后重新触发构建|
|Git Commit信息是否包含修改分类？|**feat/fix/docs/style/refactor/test**，详细说明参考[《HDF代码上库规范要求》](https://gitee.com/openharmony/drivers_hdf_core/wikis/specification/sig_driver_issue_pr_commit_review_standard)|
|Git Commit信息是否为英文？|fix: sensor data format bug|
|Pull Request标题是否包含修改分类和关键信息？|**feat/fix/docs/style/refactor/test:add xxx feature**，详细说明参考[《HDF代码上库规范要求》](https://gitee.com/openharmony/drivers_hdf_core/wikis/specification/sig_driver_issue_pr_commit_review_standard)|
|Pull Request标题描述是否全部使用英文？||
|Pull Request是否使用了默认模板？||
|Pull Request是否关联了Issue？||
|Pull Request是否包含了修改点？||
|Pull Request的验证报告是否正确？||
