# MY-OpenHarmony 文档目录

## audio模块文档
主要内容：<br>
1 设计方案，驱动模型等
## dsoftbus模块文档
主要内容：<br>
1 设计方案，驱动模型等
## sensor模块文档
主要内容：<br>
1 [sensor各器件驱动开发指导](./sensor/README.md)
## sig 文档
主要内容：<br>
1 [Drivers SIG HDI接口评审材料模板](./sig/Drivers%20SIG%20HDI接口评审材料模板V1.0.pptx)

## test文档
主要内容：<br>
1 [包括HDF开发自测试指导](./test/readme.md)