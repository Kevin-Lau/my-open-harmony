# softbus hap 发现可视化工具开发
## 【需求背景】
基于软总线开发框架，使用设备发现接口能力，开发可视化设备发现工具，提示设备发现验证效率，看护设备发现时延性能。
## 【输入】
使用OpenHarmony的ability或者扩展ability能力，开发softbus hap应用包，支持调用发现模块接口
## 【处理】
1、使用三方hap开发softbus hap应用包，hap包安装显示界面如下<br>
<div  align="center"> <img src = "./figure1.png"></div> <br>

<div  align="center"> 图1</div> <br> 
2、调用发现接口，提供给HAP进行设备发布和设备发现<br>

接口定义路径[接口定义](https://gitee.com/openharmony/communication_dsoftbus/blob/master/interfaces/kits/bus_center/softbus_bus_center.h)<br>
| 序号 | 接口定义 | 说明 |
|--- |---|---|
| 1  | int32_t PublishLNN(const char *pkgName, const PublishInfo *info, const IPublishCb *cb) | 设备发布接口 |
|2  | int32_t StopPublishLNN(const char *pkgName, int32_t publishId) | 停止设备发布接口 |
|3  | int32_t RefreshLNN(const char *pkgName, const SubscribeInfo *info, const IRefreshCallback *cb) | 设备发现接口 |
|4  | int32_t StopRefreshLNN(const char *pkgName, int32_t refreshId) | 设备发现接口 |

3、设备发布和发现流程图<br>
<div  align="center"> <img src = "./figure2.png"></div> <br>
<div  align="center"> 图2</div> <br> 
4、统计设备发现时长<br>
  在A设备上，通过主动发现接口和设备上报接口，打点统计（图2 绿色点）每次设备发现的时间，并输出在可视化界面上。<br>

## 【输出】
1、提供可视化界面HAP包
2、可视化界面提供设备发现和设备发布能力，参数可选配
3、统计每次设备发现时间长，并打印可视化界面

## 【需求优先级】
中
## 【需求难度】
中
## 【需求工作量】
6人月
## 【需求交付时间】
2024年12月30日