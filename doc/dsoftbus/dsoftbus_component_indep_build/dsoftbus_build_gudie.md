# 部件独立编译使用指南

前言：本文用于指导已经通过独立编译验收通过的部件方本地编译使用。目前部件独立编译仅支持开源社区master分支的standard形态，其他形态暂不支持；另外多仓联合编译已上线。

## 1、全量源码编译：

- 全量源码编译实测在本地虚拟机最高配置环境下编译时长最高可达11h，平均7h左右，在计算云环境下，最快也要2h，在高配置流水线下，最快也要40min+以上。

- 目前全量代码占用磁盘100G以上，拉取代码平均耗时1h左右

- 随着代码不断增加，磁盘占用越来越大，严重损耗资源环境

全量源码编译在极大的浪费资源的基础上，也严重影响开发人员进行代码编写验证。

## 2、部件独立编译

- 单仓可达秒级，最慢分钟级。

- 只编译所需要的仓库代码，所依赖的其他部件仓代码以二进制的方式引入，所以拉取代码可达分钟级

- 只增加单仓代码量，所占用磁盘环境不会显著增加

部件独立编译能够在不浪费资源环境的基础上，也方便的支撑开发人员进行代码编写验证。

如果环境已编译过全量源码，步骤一中前两步可以跳过，如果已安装 hb 工具，第三步也可跳过，否则请按照第三步安装 hb 工具。

##  **步骤一** 

### 1、准备开发环境

https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-pkg-prepare.md

### 2、安装库和工具集

https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-pkg-install-package.md

### 3、安装hb编译工具（需要先把build仓拉下来）

https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-pkg-install-tool.md


## **步骤二** 


下面以验证 **communication_dsoftbus** 部件独立编译举例

### 场景1：新建目录使用

1.初始化 repo 环境， repo 工具安装下载可参考获取源码中的”[准备工作](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-pkg-sourcecode.md)“部分

```
repo init -u https://gitee.com/openharmony/manifest.git -b master --no-repo-verify
```

2.拉取 build 仓和独立编译部件 dsoftbus 对应的仓代码，build仓是编译必备代码仓，验证其他部件则使用其他部件对应的仓库名称替换下方的 communication_dsoftbus，因为 dsoftbus部件在gitee上的仓库是 communication_dsoftbus，所以拉取communication_dsoftbus仓代码

```
repo sync -c build communication_dsoftbus
```

3.执行独立编译使用的编译器和二进制工具脚本。如果业务代码编译过程中需要依赖sdk里面的内容，就在脚本后面添加--download-sdk下载即可，如果不需要就不用添加，是一个可选项。（目前频繁迭代时期，建议半个月执行下这个脚本，目的是更新hpm客户端）

**注意**：`--download-sdk`命令会增加约5分钟下载时长。

```
bash build/prebuilts_config.sh
```

4.执行部件独立编译源码命令，hb build {部件名} -i，-i 只能放到部件名的后面；执行部件测试用例独立编译命令， hb build {部件名} -t，同样的，-t也只能放在部件名的后面。以上这两条命令按需执行即可。

```
hb build dsoftbus -i (编译源码)
hb build dsoftbus -t (编译测试用例)
```
### 场景2：在全量源码下使用
执行独立编译使用的编译器和二进制工具脚本。如果业务代码编译过程中需要依赖sdk里面的内容，就在脚本后面添加--download-sdk下载即可，如果不需要就不用添加，是一个可选项。（目前频繁迭代时期，建议半个月执行下这个脚本，目的是更新hpm客户端）
**注意**：`--download-sdk`命令会增加约5分钟下载时长。
```
bash build/prebuilts_config.sh
```
在源码目录直接执行部件独立编译命令即可，以下这两条命令按需执行即可。 -i/-t只能放在部件名后面
```
hb build dsoftbus -i (编译源码)
hb build dsoftbus -t (编译测试用例)
```


![](../img/case19-1.png)

### 拓展功能
在以上两种场景中，还可以使用多仓联合编译功能，使用前提是部件A、部件B、部件C均已完成部件独立编译整改验收。
```
hb build 部件A 部件B 部件C -i （编译源码）
hb build 部件A 部件B 部件C -t （编译测试用例）
```

### 带符号表的so，验证crash
```
so的路径是out/default/src下的lib.unsripperd中
out/default/test下的lib.unsripperd中
```


