# Open Harmony HDF驱动自测试开发指导系列--系列二用户态篇

 

- [1、用户态自测试框架介绍](##1、用户态自测试框架介绍)
- [2、用户态测试用例开发步骤](##2、用户态测试用例开发步骤)
  - [2.1编写测试用例](###2.1编写测试用例)
  - [2.2编写编译脚本](###2.2编写编译脚本)
  - [2.3编译脚本入口适配](###2.3编译脚本入口适配)
  - [2.4依赖的资源文件配置](###2.4依赖的资源文件配置)

- [3、用例测试](##3、用例测试)

 

## 1、用户态自测试框架介绍

​        用户态自测试框架采用Gtest测试框架，测试套文件需要继承testing::Test类。通过测试套调用被测函数实现用例测试，测试用例的预期结果必须要有对应的断言，来判断测试结果是否与预期结果一致。

## 2、用户态测试用例开发步骤

​       以标准系统中Hi3516DV300产品为例，介绍用户态测试用例开发。

### 2.1编写测试用例

1.按照测试用例目录规划，定义测试套文件

​        HDF驱动功能测试时，一般外设测试套定义路径为drivers\peripheral仓各模块test目录下，HDF框架功能测试套定义路径为drivers\framework仓下各模块test目录下，如下示例所示。

```
└── test
    └── unittest
        ├── BUILD.gn
        └── common
            └── hdf_sensor_test.cpp
```

​        测试套文件路径为HDF驱动功能测试时，一般外设测试套定义路径为drivers\peripheral仓各模块test目录下（例如drivers\peripheral\audio\test\unittest\common），HDF框架功能测试套定义路径为drivers\framework仓下各模块test目录下(例如support\platform\test\unittest\common)。

​        测试套文件需要继承testing::Test类，命名以测试模块+test.cpp进行命名，来定义测试套文件。因此，开发一个外设用户态测试用例，可参考hdf_sensor_test.cpp实现，新建hdf_xxx_test.cpp,实现自己模块的测试套。

2.引用gtest头文件和ext命名空间，具体实现如下示例所示：

```
#include <gtest/gtest.h>
using namespace testing::ext;
```

3.定义测试套（测试类）

​        实现测试套时需要先进行预处理和后处理操作（如下示例所示），（具体实现方法可参照hdf_sensor_test.cpp文件），SetUpTestCase是针对该测试套下所有的用例执行前的预置处理；TearDownTestCase是针对该测试套下所有的用例执行后释放。

```
void HdfSensorTest::SetUpTestCase()
{
    g_sensorDev = NewSensorInterfaceInstance();
    if (g_sensorDev == nullptr) {
        printf("test sensorHdi get Module instance failed\n\r");
    }
}
void HdfSensorTest::TearDownTestCase()
{
    if (g_sensorDev != nullptr) {
        FreeSensorInterfaceInstance();
        g_sensorDev = nullptr;
    }
}
```

4.实现测试套具体的测试用例，包括用例注释和用例的逻辑实现

​        针对被测对象的特性编写测试用例，以hdf_sensor_test.cpp文件为例说明。

用例注释是在编写测试用例时，先要对以下信息进行备注：

@tc.name：用例名称 

@tc.desc：描述用例详细描述，包括测试目的、测试步骤、期望结果等。

@tc.type：测试属性分类（FUNC（功能测试）、PERF（性能测试）、SECU（安全测试）、RELI（可靠性测试））。

@tc.require：需求编号 

​        逻辑实现：调用测试套HWTEST_F编写测试用例（Gtest框架中常用的测试套种类及功能请参考[测试用例开发指导](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/subsystems/subsys-testguide-test.md)的开发指导篇章中的接口说明章节），测试用例的预期结果必须要有对应的断言，来判断测试结果是否与预期结果一致，（常用断言功能请参考[Open Harmony HDF驱动自测试开发指导系列--系列一测试框架篇.docx](Open Harmony HDF驱动自测试开发指导系列--系列一测试框架篇.docx)的测试用例介绍篇章中的常用断言介绍章节），如ASSERT_NE、EXPECT_EQ。HWTEST_F测试套编写的用例具备独立性，执行过程相互之间不受影响。

```
/**
  * @tc.name: GetSensorInstance001
  * @tc.desc: Create a sensor instance and check whether the instance is empty.
  * @tc.type: FUNC
  * @tc.require: SR000F869M, AR000F869N, AR000F8QNL
  */
HWTEST_F(HdfSensorTest, GetSensorInstance001, TestSize.Level1)
{
    ASSERT_NE(nullptr, g_sensorDev);
    const struct SensorInterface *sensorDev = NewSensorInterfaceInstance();
    EXPECT_EQ(sensorDev, g_sensorDev);
}
```

### 2.2编写编译脚本

​       用例编译的BUILD.gn路径在drivers/peripheral仓下的每个模块下test目录下。

​       编写BUILD.gn文件，以sensor为例进行说明：

1.导入用例编译模板文件；

```
import("//build/ohos.gni")
import("//build/test.gni")
```

2.指定用例文件的输出路径；

```
module_output_path = "hdf/sensor"
```

3.编写具体的用例编译脚本（添加需要参与编译的源文件、配置和依赖）；

```
sources = [ "./common/hdf_sensor_test.cpp" ]
    cflags = [
      "-Wall",
      "-Wextra",
     ......
    ]
    deps = [
      "$hdf_uhdf_path/osal:libhdf_utils",
      "//drivers/peripheral/sensor/hal:hdi_sensor",
      "//utils/native/base:utils",
 ]
```

注：测试用例编译配置规范：测试用例采用GN方式编译，配置遵循本开源项目的[编译构建](https://gitee.com/openharmony/build)。

### 2.3编译脚本入口适配

​        路径：\drivers\adapter\uhdf2路径下的test目录下的ohos.build文件。每新增一个模块的测试用例编译脚本，都需在此文件中的"test_list"下新增脚本入口，具体实现如下示例所示。

```
{
  "subsystem": "hdf",
  "parts": {
    "hdf": {
      "module_list": [
        ......
      ],
      "test_list": [
        "//drivers/peripheral/wlan/test:hdf_test_wifi",
        ......
      ]
    }
  }
}
```

### 2.4依赖的资源文件配置

​        测试依赖的配置文件放置路径\drivers\adapter\uhdf2\test\resource中各模块目录下ohos_test.xml文件，具体实现可参考同目录下的其他文件，如下示例所示：

```
<configuration ver="2.0">
    <target name="hdf_adapter_uhdf_test_platform"> // 测试单元的名字
        <preparer>
            <option name="push" value="hdf/hdf/libhdf_test_common.z.so -> /system/lib" src="out"/> // 编译输出后测试资源位于out/release/$(子系统名)目录
        </preparer>
    </target>
</configuration>
```



## 3、用例测试

​          具体测试步骤可参考[HDF测试指导](https://gitee.com/Kevin-Lau/my-open-harmony/wikis/hdf自测试介绍?sort_id=4409313)。