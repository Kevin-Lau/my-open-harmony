
## 布尔值检查
| 致命断言 | 非致命断言 | 条件 |
|---------|-----------|------|
|ASSERT_TRUE(condition)|EXPECT_TRUE(condition)|conditionis true|
|ASSERT_FALSE(condition)|EXPECT_FALSE(condition)|conditionis false|

## 数值型数据检查
| 致命断言 | 非致命断言 | 条件 |
|---------|-----------|------|
|ASSERT_EQ(expected,actual)|EXPECT_EQ(expected,actual)|expected==actual|
|ASSERT_NE(val1,val2)|EXPECT_NE(val1,val2)|val1!=val2|
|ASSERT_LT(val1,val2)|EXPECT_LT(val1,val2)|val1<val2|
|ASSERT_LE(val1,val2)|EXPECT_LE(val1,val2)|val1<=val2|
|ASSERT_GT(val1,val2)|EXPECT_GT(val1,val2)|val1>val2|
|ASSERT_GE(val1,val2)|EXPECT_GE(val1,val2)|val1>=val2|

## 字符串检查
| 致命断言 | 非致命断言 | 条件 |
|---------|-----------|------|
|ASSERT_STREQ(expected_str,actual_str)|EXPECT_STREQ(expected_str,actual_str)|the two C strings have the same content|
|ASSERT_STRNE(str1,str2)|EXPECT_STRNE(str1,str2)|the two C strings have different content|
|ASSERT_STRCASEEQ(expected_str,actual_str)|EXPECT_STRCASEEQ(expected_str,actual_str)|the two C strings have the same content, ignoring case|
|ASSERT_STRCASENE(str1,str2)|EXPECT_STRCASENE(str1,str2)|the two C strings have different content, ignoring case|