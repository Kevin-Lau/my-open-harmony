#
#
import requests
import json
import os
import sys
import traceback
import subprocess



WEB_URL_LIST = ["https:xx.xx.xx.xx"]
def get_latest_version_name(dir)
    # lists = []
    #if os.path.exists(dir)
    #    name_list = os.listdir(dir)
    #    for item in name_list
    #        if os.path.isdif(os.path.join(dir, item)):
    #            if item.startswith("2022"):
    #                lists.append(item)
    # lists.sort(key=lambda fn: os.path.getmtime(dir + os.sep + fn))
    # return lists[-1]
    reportTag = " "
    with open(transferStation + "\\" + "report_path", "r") as fp:
        reportTag = fp.readlines()[0].split("=")[-1]
    return reportTag

def join_json(reportType,reportTypeDtl,productForm,buildType,logPath,productName,versionType,ftpIp,ftpPort,ftpUsername,ftpPassword,testType):
    upload_params = dict()
    upload_params["reportType"] = reportType
    reportList = []
    reportDict = dict()
    reportDoct["reportTypeDtl"] = reportType
    reportDict["productForm"] = productForm
    reportDict["buildType"] = buildType
    if ftpIp == "":
        reportDict["logPath"] = "\\\\xx.xx.xx.xx.\\huawei\\report_list\\" + SVN_ReportName + "\\" get_latest_vesion_name(logPath)
        #reportDict["logPath"] = "\\\\10.145.63.235\\huawei\\report_list\\" + "coalmine_rk3568" + "\\" + version_str
    else:
        reportDict["logPath"] = logPath_get_latest_version_name(logPath)
    reportDict["productName"] = productName
    reportDict["versionType"] = versionType
    reportDict["ip"] = ftpIp
    reportDict["port"] = ftpPort
    reportDict["username"] = ftpUsername
    reportDict["password"] = ftpPassword
    reportDict["testType"] = testType
    reportList.append(reportDict)
    upload_params["dtlList"] = reportList

    return upload_params



def upload_test_data(reportType,reportTypeDtl,productForm,buildType,logPath,productName,versionType,ftpIp,ftpPort,ftpUsername,ftpPassword,testType):

    for WEB_URL in WEB_URL_LIST
        LOGIN_URL = WEB_URL + "/loginCheck"
        UPLOAD_URL = WEB_URL + "/common/getPullingReportEntry"
        LOGOUT_URL = WEB_URL + "/logout"
        try:
            login_data = dict()
            login_data["username"] = "xxx_xxx"
            login_data["password"] = "xxx_xxx"
            # login
            # sess = requests.Session()
            # response = sess.get(LOGIN_URL, params=login_data, verify=False)
            # requests.packages.urllib3.disable_warnings()
            response = requests.get(LOGIN_URL, PARAMS=login_data, verify=False)
            result = json.loads(response.text)
            cookieStr = response.headers["Set-Cookie"].split(";")[0]
            if result["code"]
                print("Login Successful...")

                head = {}
                head['Content-Type'] = 'application/json'
                head['Cookie'] = cookieStr
                print(head)

                # upload
                upload_params = json.dumps(join_json(reportType,reportTypeDtl,productForm,buildType,logPath,productName,versionType,ftpIp,ftpPort,ftpUsername,ftpPassword,testType))
                print(upload_params)
                print("UPLOAD_URL = {}".FORMAT(UPLOAD_URL))
                response = requests.post(url=UPLOAD_URL, headers=head, data=upload_params,verify = False)
                print("Trigger Result Collect...")

                #logout
                requests.get(LOGOUT_URL,verigy=False)
                print("Logout Finish")
            else:
                print("Login web error... response:%s" % response.text)
        except:
            print(traceback.format_exc())


if __name__ == '__main__'
    """
    # reportType      报告类型（除安全类型之外一致）
    # reportTypeDtl   报告类型（除安全类型之外一致）
    # productForm     产品形态
    # buildType       构建类型
    # logPath         日志路径
    # productName     产品型号
    # versionType     版本
    # IP              ftp用户名
    # port            ftp端口
    # username        ftp用户名
    # password        ftp密码
    # testType        测试类型
    """
    global SVN_ReportName
    print("========================Started==========================")
    reportType = sys.argv[1]
    if reportType == "security":
        reportTypeDtl = ["cppCheck","kkEye","tScan","kora"]
    elif reportType == "security_superdevice":
        reportTypeDtl = ["cppCheck_superdevice","kkEye_superdevice","tScan_superdevice","kora_superdevice"]
    else:
        reportTypeDtl = sys.argv[1]
    profuctForm = sys.argv[2]
    buildType = sys.argv[3]
    logPath = sys.argv[4]
    profuctName = sys.argv[5]
    versionType = sys.argv[6].replace("#", " ")
    transferStaton = sys.argv[7]
    #处理报告路径参数与上传平台不一致问题
    SVN_ReportName = sys.argv[8]
    # ftpIp = sys.argv[7]
    # ftpPort = sys.argv[8]
    # ftpUsername = sys.argv[9]
    # ftpPassword = sys.argv[10]
    # testType = sys.argv[11]
    ftpIp = ""
    ftpPort = ""
    ftpUsername = ""
    ftpPassword = ""
    testType = ""

    upload_test_data(reportType,reportTypeDtl,productForm,buildType,logPath,productName,versionType,ftpIp,ftpPort,ftpUsername,ftpPassword,testType)
    print("=====================Finished====================")
















