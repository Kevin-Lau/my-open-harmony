import os, sys
import time
import datetime
import smtplib
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.utils import COMMASPACE, formatdate

class EmailSender(object):
    def __init__(self):
        # Func: 初始化函数
        self.server = smtplib.SMTP()
        self.mailhost = "smtpscn1.huawei.com:25"
        self.mailusername = "pmail_pAndriod"
        self.mailpassword = "jenkins_2013"

    def set_host(self, mail_host, mail_username, mail_password):
        # mail_host: SMTP的地址和端口信息
        # mail_username: 登陆用户名

        if self.mailhost == "" or self.mailusername == "" or self.mailpassword == "":
            return False

        # 连接SMTP服务器
        try:
            self.server.connect(self.mailhost)
            self.server.login(self.mailusername, self.mailpassword)
            return True
        except Exception as e:
            print(str(e))
            return False

    def send_mail(self, mail_from, mail_receivers, mail_tocc, mail_subject, mail_content,
                  subtype="plain", charset=None):
        # mail_from: 发件人
        # mail_receivers: 收件人
        # mail_tocc: 抄送人
        # mail_subject: 邮件主题
        # mail_content: 邮件内容

        if mail_from == "" or mail_subject == "" or mail_content == "" or len(mail_receivers) == 0:
            return False

        # 设置发送信息
        mail_msg = MIMEText(mail_content, _subtype=subtype.lower(), _charset=charset)
        mail_msg['From'] = mail_from
        mail_msg['To'] = ";".join(mail_receivers)
        mail_msg['Cc'] = ";".join(mail_tocc)
        mail_msg['Subject'] = mail_subject

        # 发送邮件
        try:
            self.server.sendmail(mail_from, mail_receivers, mail_msg.as_string())
            self.server.close()
            return True
        except Exception as e:
            print(e)
            return False

    def send_mail_with_attachment(self, mail_from, mail_receivers, mail_tocc, mail_subject,
                                  mail_content, attach_list, subtype="plain", charset=None):

        if mail_from == "" or mail_subject == "" or mail_content == "" or len(mail_receivers) == 0:
            return False

        # 设置发送信息
        mail_msg = MIMEMultipart('alternative')
        mail_msg['From'] = mail_from
        mail_msg['To'] = ";".join(mail_receivers)
        mail_msg['Cc'] = ";".join(mail_tocc)
        mail_msg['Subject'] = mail_subject

        # 添加发送人与抄送人
        mail_receAll = mail_receivers + mail_tocc

        # 添加邮件内容信息
        part = MIMEText(mail_content, _subtype=subtype.lower(), _charset=charset)
        mail_msg.attach(part)

        # 构造邮件附件
        for item in attach_list:
            # 判断文件是否存在
            if False == os.path.exists(item):
                continue

            # 添加附件
            attFileDir, attFileName = os.path.split(item);
            part = MIMEApplication(open(item, 'rb').read())
            part.add_header('Content-Disposition', 'attachment', filename=attFileName)
            mail_msg.attach(part)

        # 发送邮件
        try:
            self.server.sendmail(mail_from, mail_receAll, mail_msg.as_string())
            self.server.close()
            return True
        except Exception as e:
            print(e)
            return False

    def send_mail_message(self, mailFrom, mailRece, mailTo, mailSubject, mailContent, mailAttachPath):
        # mailAttachPath:  附件文件路径
        if mailFrom == "" or mailSubject == "" or mailContent == "" or len(mailRece) == 0:
            return

        mailAttachList = []
        if os.path.exists(mailAttachPath) == True:
            mailAttachList.append(mailAttachPath)

        # 测试发送带附件的邮件
        print("Start sending email.")
        self.set_host(self.mailhost, self.mailusername, self.mailpassword)
        self.send_mail_with_attachment(mailFrom, mailRece, mailTo, mailSubject, mailContent, mailAttachList, subtype="html", charset="utf-8")

        time.sleep(3)


if __name__= "_main__:
    print("************************Started*********************")

    if len(sys.argv) > 1:
        product = sys.argv[1]

        mailFrom = "pandriod@huawei.com"
        mailRece = ["chenyule@huawei.com"]
        mailToCC = ["chenyule@huawei.com"]

        strFormatData = time.strftime("%Y-%m-%d")
        resultFilePath = "D:\\jenkins\\visualtool\\temp\\%s-Result.txt" % (product)
        hFile = open(resultFilePath, "rb")
        strResult = hFile.read()
        hFile.close()

        mailSubject = "OS PDK" + product + "产品流水线执行情况" + strFormatData + strResult
        mailContent = "#hello, eSpace自动群发信息，请勿回复，谢谢"

        reportList = {
            "STF": "../../temp/STFReport.html",
            "TRT": "../../temp/TRTReport.html",
            "MHA": "../../temp/MHAReport.html",
            "WAS": "../../temp/WASReport.html",
        }

        if reportList.has_key(product):
            mailAttachPath = reportList.get(product)

            fp = open(mailAttachPath, 'rb')
            mailContent = fp.read()
            fp.close()

            sender = EmailSender()
            sender.send_mail_message(mailFrom, mailRece, mailToCC, mailSubject, mailContent, mailAttachPath)

    print("************************Finished*********************")