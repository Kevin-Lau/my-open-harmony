# -*- coding:utf-8 -*-
# Author:

import os
import time
import shutil
from collections import OrderedDict

def getComponentsName(path):
    # path: 测试套路径
    # Func: 获取指定路径下的部件名

    # 部件列表
    component_list = []
    # 测试套路径列表
    files_list = []
    for dirpath, dirnames, filenames in os.walk(path):
        component_list = dirnames
        if component_list!=[]:
            break
    for dirpath, dirnames, filenames in os.walk(path):
        if "resource" not in dirpath:
            for special_file in filenames:
                files_list.append(os.path.join(dirpath, special_file))
    return component_list

def getComponentsAndSuiteKV(path):
    # path: 测试套路径
    # Func: 获取部件对应的测试套字典

    # 部件与测试套对应字典
    suite_dict = {}
    # 部件名列表
    component_list = []
    # 测试套路径列表
    files_list = []
    for dirpath, dirnames, filenames in os.walk(path):
        component_list = dirnames
        if component_list!=[]:
            break
    for dirpath, dirnames, filenames in os.walk(path):
        if "resource" not in dirpath:
            for special_file in filenames:
                files_list.append(os.path.join(dirpath, special_file))
    for c_item in component_list:
        suite_list = []
        for f_item in files_list:
            if c_item in f_item:
                print(f_item.split("\\")[-1])
                suite_list.append(f_item.split("\\")[-1])
        suite_dict[c_item] = suite_list
    return suite_dict

def runCasesWithComponents(path, component_lst = [], test_type = "UT"):
    # path: 测试套路径
    # component_lst: 组件列表
    # test_type: 测试类型
    # Func: 以部件为单元进行测试

    try:
        os.chdir(path)
        for item in component_lst:
            run_cmd = "run -t " + test_type + " -ss " + item + "\n"
            print("OpenHarmony: " + run_cmd)
            with os.popen("start.bat", "w") as finput:
                finput.write("1\n")
                finput.write(run_cmd)
                finput.write("quit\n")
                finput.write("exit(0)\n")
            # 此部分挪至start.bat脚本中处理
            # os.system("hdc_std shell reboot")
            # os.system("hdc_std kill")
            # time.sleep(5)
            # os.system("hdc_std list targets")
    except Exception as e:
        return "runCasesWithComponents happened exception!"

def runCasesWithSuite(path, componentName, suite_lst = [], test_type = "UT"):
    # path: 测试套路径
    # componentName: 组件名称
    # suite_lst: 测试套列表
    # test_type: 测试类型
    # Func: 以测试套为单元进行测试

    try:
        os.chdir(path)
        for item in suite_lst:
            run_cmd = "run -t " + test_type + " -ss " + componentName + " -ts " + item + "\n"
            print("OpenHarmony: " + run_cmd)
            with os.popen("start.bat", "w") as finput:
                finput.write("1\n")
                finput.write(run_cmd)
                finput.write("quit\n")
                finput.write("exit(0)\n")
    except Exception as e:
        return "runCasesWithSuite happened exception!"

def delFolder(path):
    # path: 路径
    # Func: 删除指定目录

    if os.path.exists(path):
        try:
            shutil.rmtree(path)
        except PermissionError:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        except OSError:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        except BaseException:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)
        finally:
            time.sleep(10)
            if os.path.exists(path):
                shutil.rmtree(path)

def createFolder(path):
    # path: 路径
    # Func: 创建指定目录

    try:
        if not os.path.exists(path):
            os.makedirs(path)
    except PermissionError:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    except OSError:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    except BaseException:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)
    finally:
        time.sleep(10)
        if not os.path.exists(path):
            os.makedirs(path)

def AnnotateExe(file_dir, exe_name = []):
    # file_dir: 路径
    # exe_name: 屏蔽测试套列表
    # Func: 删除特定测试套

    for root, dirs, files in os.walk(file_dir):
        for file in files:
            for exe in exe_name:
                if exe in file:
                    print("*" * 20)
                    print("remove: %s" %os.path.join(root, file))
                    print("*" * 20)
                    os.remove(os.path.join(root, file))

def reOrderBin(dict, subSys):
    # dict: 字典集合
    # subSys: 子系统名称
    # Func: 无序执行集合，调整为有序

    ordLst = []
    if "-" in subSys:
        ordLst = subSys.split("-")
    else:
        ordLst.append(subSys)
    ordDict = OrderedDict()
    initDict = OrderedDict(dict)
    delKeyDict = OrderedDict(dict)
    print("##############order_dict##############")
    print("initDict len: %s" %len(initDict))
    print("##############order_dict##############")
    for item in ordLst:
        for k, v in initDict.items():
            if item == k:
                ordDict[k] = v

    for item in ordLst:
        for k, v in delKeyDict.items():
            if item == k:
                del initDict[item]

    for k, v in initDict.items():
        ordDict[k] = v

    print("##############order_dict##############")
    print("ordDict len: %s" % len(ordDict))
    print("##############order_dict##############")
    for k, v in ordDict.items():
        print("Components Name: %s" %k)
    return ordDict