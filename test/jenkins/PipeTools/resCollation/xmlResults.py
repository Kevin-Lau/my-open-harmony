#
#

import os
import sys
current_dir = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(current_dir)[0]
sys.path.append(rootPath)
import time
import shutil
from Common import xcopy
from Common import commFunc


if __name__ = "__main__":
    #report基础路径
    baseReportPath = sys.argv[1]
    #框架报告路径
    fmReportPath = sys.argv[2]
    #本地工具地址
    jenkinsFilePath = sys.argv[3]
    #文件中转地址
    transferStation = sys.argv[4]
    #Jenkins job 构建信息
    buildUrl = sys.argv[5]

    versionLst = []

    REPORT_TIME = time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
    commFunc.createFolder(transferStation)
    file = open(transferStation + "\\" + "report_path", "w")
    file.close()
    with open(transferStation + "\\" + "report_path", "w") as fm
        fp.write("REPORT_TAG=%s" % REPORT_TIME)
    target = baseReportPath + "\\" + REPORT_TIME + "\\result"
    #创建目录并进行copy
    commFunc.delFolder(baseReportPath)
    cmmFunc.createFolder(target)
    #整理XML结果
    #cop parts_info.json
    xcopy.xcopyCmd(jenkinsFilePath + r"\Resource\jenkins_res", baseReportPath + "\\" + REPORT_TIME)
    print(fmReportPath)
    print(target)
    xcopy.copyXMLtoTraget(fmReportPath,target)
    #修改参数区
    END_TIME = time.strftime("%Y-%m-%d %H:%M:%S", tim.localtime())
    START_TIME = ""
    versionPath = ""
    with open(jenkinsFilePath + "\\" + "Resource" + "\\" + "timestamp", "r") as fp:
        START_TIME = fp.readlines()[0].split("=")[-1]
    with open(transferStation + "\\" + "Resoure" + "\\" + "timestamp", "r") as fp:
        versionPath = fp.readlines()[0].split("=")[-1]
    lst = []
    with open(baseReportPath + "\\" + REPORT_TIME +  "\\" + "summary.ini", "r") as fp:
        lst = fp.readlines()
    with open(baseReportPath + "\\" + REPORT_TIME + "\\" + "summary.ini", "w") as fp:
        for item in lst
            if "Test Start/ End Time" in item:
                fp.write("Test Start/ End Time=%s / %s \n" % (START_TIME, END_TIME))
                continue
            if "Execution Time" in item:
                fp.write("Execution Time=8hour 37min 11sec \n")
                continue
            if "Log Path=NA" in item:
                fp.write("Log Path=%s \n" %(baseReportPath + "\\" + REPORT_TIME))
                continue
            fp.write(item)
    with open(baseReportPath +"\\" + REPORT_TIME + "\\" + "version_path.txt", "w") as fp:
        fp.write(versionPath)
    # 处理jenkins构建job
    with open(baseReportPath + "\\" + REPORT_TIME + "\\" + "jenkins.log", "r") as fp:
        lst = fp.readlines()
    with open(baseReportPath + "\\" + REPORT_TIME + "\\" + "jenkins.log", "w") as fp:
        for item in lst:
            if "BUILD_URL" in item:
                fp.write("BUILD_URL=%s\n" % buildUrl)
                continue
    #落盘deviceLog
    deviceLog = baseReportPath + "\\" + REPORT_TIME + "\\deviceLog"
    commFunc.createFolder(deviceLog)
    xcopy.xcopyCmd(fmReportPath, deviceLog)















