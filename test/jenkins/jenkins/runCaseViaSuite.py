import os
import sys
import time
import shutil
current_dir = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(current_dir)[0]
sys.path.append(rootPath)
from Common import commFunc
from Common import xcopy

if __name__ == "__main__":
    developtest_path = sys.argv[1]
    test_type = sys.argv[2]
    testPath = sys.argv[3]
    toolPath = sys.argv[4]
    
    
    print("=======================================================================")
    print("======================== copy partsInfo ===============================")
    print("=======================================================================")
    reports_local_dir = developtest_path + r"\reports"
    commFunc.delFolder(reports_local_dir)
    commFunc.createFolder(reports_local_dir)
    
    with open(toolPath + "\\" + r"Resource\timestamp", "w") as fp:
        fp.write("START_TIME=%s" %time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))


    print("=======================================================================")
    print("========================== run testSuite ==============================")
    print("=======================================================================")
    type_lst = []
    for item in  test_type.split("-"):
        print(item)
        type_lst.append(item)
    for item in type_lst:
        if item == "unittest":
            print("~~Start to test unittest~~")
            dict = commFunc.getComponentsAndSuiteKV(path=testPath + "\\unittest")
            print(str(dict))
            for k, v in dict.items():
                commFunc.runCasesWithSuite(developtest_path, k, v, "UT")
        if item == "moduletest":
            print("~~Start to test moduletest~~")
            dict = commFunc.getComponentsAndSuiteKV(path=testPath + "\\moduletest")
            #for k, v in commFunc.reOrderBin(dict, subSys).itmes():
                #commFunc.runCasesWithSuite(developtest_path, k, v, "MST")
        if item == "systemtest":
            print("~~Start to test systemtest~~")
            dict = commFunc.getComponentsAndSuiteKV(path=testPath + "\\systemtest")
            #for k, v in commFunc.reOrderBin(dict, subSys).itmes():
                #commFunc.runCasesWithSuite(developtest_path, k, v, "ST")