
@rem del %CD%\hdf_unittest_sensor
@rem ping -n 3 127.0.0.1>nul
@rem copy Z:\code\0125\out\hi3516dv300\tests\unittest\hdf\sensor\hdf_unittest_sensor %CD%\

hdc kill

hdc shell mount -o rw,remount /

@ping -n 3 127.0.0.1>nul

hdc file send %CD%\hdi_unittest_sensor /

hdc shell chmod 777 hdi_unittest_sensor

hdc shell ./hdi_unittest_sensor > %CD%\111.txt

@ping -n 3 127.0.0.1>nul

pause