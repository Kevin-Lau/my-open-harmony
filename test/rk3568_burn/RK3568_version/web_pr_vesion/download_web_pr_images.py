import json
import os
import time

import requests
import collections

# 进度条模块
def progressbar(url, file_name):
    # if not os.path.exists(path):   # 看是否有该文件夹，没有则创建文件夹
    #      os.mkdir(path)
    #  下载开始时间
    start = time.time()
    response = requests.get(url, stream=True)
    #  初始化已下载大小
    size = 0
    #  每次下载的数据大小
    chunk_size = 1024
    content_size = int(response.headers['content-length'])  # 下载文件总大小
    try:
        #   判断是否响应成功
        if response.status_code == 200:
            #  开始下载，显示下载文件大小
            print('Start download,[File size]:{size:.2f} MB'.
                  format(size=content_size / chunk_size / 1024))
            # 设置图片name，注：必须加上扩展名
            # 显示进度条
            with open(file_name, 'wb') as file:
                for data in response.iter_content(chunk_size=chunk_size):
                    file.write(data)
                    size += len(data)
                    print('\r'+'[下载进度]:%s%.2f%%'
                          % ('>'*int(size*50 / content_size),
                             float(size / content_size * 100)),
                             end=' ')
        end = time.time()   # 下载结束时间
        print('Download completed!,times: %.2f秒' % (end - start))  #输出下载用时时间
    except:
        print('Error!')


if __name__ == '__main__':
    import re
    import os
    import sys
    url = sys.argv[1]
    print(url)
    create_time = re.search(r"/[0-9]+\.[0-9]+/", url).group().strip('/')
    cur_path = os.getcwd()
    cur_date_path = os.path.join(cur_path, "version_" + create_time)
    if not os.path.exists(cur_date_path):
        os.mkdir(cur_date_path)
    file_name = os.path.join(cur_date_path, url.split("/")[-1])
    print(file_name)
    progressbar(url=url, file_name=file_name)
#   python down_code.py http://download.ci.openharmony.cn/Artifacts/dayu200/20220421.1224/version/Artifacts-dayu200-20220421.1224-version-dayu200.tar.gz
