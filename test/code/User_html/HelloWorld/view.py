# from django.http import HttpResponse
#
#
# def hello(request):
#     return HttpResponse("Hello world ! ")

from django.shortcuts import render, HttpResponse
from . import task
#
# def ab_ajax(request):
#     # 前端请求
#     if request.method == "POST":
#         # print(request.POST)  # <QueryDict: {'i1': ['111'], 'i2': ['222']}>
#         # 拿到接收数据 进行加法运算 在返回回去
#         i1 = request.POST.get('i1')
#         i2 = request.POST.get('i2')
#         # 先转成整型再加
#         i3 = int(i1) + int(i2)
#         print(i3)
#
#         # 执行版本烧录
#         # rk3568.burn_rk3568()
#         if i1 == i2:
#             # rk3568.burn_rk3568('D:\web_test_0424\HelloWorld\ccrk3568\ccversion-Daily_Version-dayu200-20220414_200131-dayu200_img',"D:\web_test_0424\HelloWorld\ccrk3568\cctool\ccugrade_tool.exe")
#             task.run_script()
#         # 返回给前端
#         return HttpResponse(i3)
#     return render(request, 'index.html')

def ab_ajax(request):
    # 前端请求
    if request.method == "POST":
        # print(request.POST)  # <QueryDict: {'i1': ['111'], 'i2': ['222']}>
        # 拿到接收数据 进行加法运算 在返回回去
        i1 = request.POST.get('i1')
        i2 = request.POST.get('i2')
        i3 = request.POST.get('i3')
        i4 = request.POST.get('i4')
        
        task.run_script(i1,i2,i3,i4)
        # 返回给前端
        return HttpResponse("success")
    return render(request, 'assembly.html')
