#
#
import sys
import time
import os
import re


import tarfile
import gzip
# import os
# import sys

# 解压第一层 gz
# def un_gz(file_name):
#     """ungz zip file"""
#     f_name = file_name.replace(".gz", "")
#     # 获取文件的名称，去掉
#     g_file = gzip.GzipFile(file_name)
#     # 创建gzip对象
#     open(f_name, "wb+").write(g_file.read())
#     # gzip对象用read()打开后，写入open()建立的文件里。
#     g_file.close()  # 关闭gzip对象

# 解压第二层tar
# def un_tar(file_name):
#     # untar zip file
#     tar = tarfile.open(file_name)
#     names = tar.getnames()
#     if os.path.isdir(file_name + "_files"):
#         pass
#     else:
#         os.mkdir(file_name + "_files")
#     # 由于解压后是许多文件，预先建立同名文件夹
#     for name in names:
#         tar.extract(name, file_name + "_files/")
#     tar.close()

# -------------------------------------------------------------------tar--------------------------------
def sendCmd(mycmd):
    result = "".join(os.popen(mycmd).readlines())
    return result

def getLatestFolder(path):
    # Func: 获取指定路径下的最新目录

    dirs = os.listdir(path)
    print("dirs = ", dirs)
    dirs.sort()
    return dirs[-1]

# --------------------------------------------------- tar--------------------------------
def tar_gz(local_dir,tar_exe):
    # local_dir  = sys.argv[1]
    # tar_exe = sys.argv[2]
    name = os.path.basename(vesion_dir)
    print(name)

    version_dir = getLatestFolder(local_dir)
    #local_dir 为sys.argv[1]中最新的文件
    local_dir = local_dir + "\\" + version_dir
    print("\n==================================local_dir Path================================")
    print(local_dir)

    if os.path.exists(local_dir + "\\" + "version_tar_dir"):
        print(local_dir + "\\" + "version_tar_dir is existed")
        du200 = local_dir + "\\"  + "version_tar_dir" + "\\"+ "du200"
    else:
        # 解压第一层
        dirs = os.listdir(local_dir)
        dirs.sort()
        print("dirs = ", dirs)

        du200_tar_gz = local_dir + "\\" + dirs[-1]
        print("\n==================================du200_tar_gz Path================================")
        print(du200_tar_gz)
        cmd_1 = tar_exe + " x " + du200_tar_gz + \
                " -y -o\jenkins_worksp\RK3568\RK3568_burn\RK3568_version" + "\\" + name + "\\" + version_dir + "\\" + "version_tar_dir"
        print(cmd_1)
        sendCmd(cmd_1)

        version_tar_dir = getLatestFolder(local_dir)
        local_dir = local_dir + "\\" + version_tar_dir

        dirs = os.listdir(local_dir)
        dirs.sort()
        print("dirs = ", dirs)

        # 解压第二层，使用本地解压工具解压。原因python解压不完全
        du200_tar = local_dir + "\\" + dirs[-1]
        print("\n==================================du200_tar Path================================")
        print(du200_tar)
        cmd_2 = tar_exe + " x " + du200_tar + \
                " -y -o\jenkins_worksp\RK3568\RK3568_burn\RK3568_version" + "\\" + name + "\\" + version_dir + "\\" + version_tar_dir + "\\" + "du200"
        print(cmd_2)
        sendCmd(cmd_2)
        du200 = local_dir + "\\" + "du200"
        print(du200)

    return du200
    # sys.exit()

# -------------------------------------------------------------------burn--------------------------------
def sendCmd(mycmd):
    result = "".join(os.popen(mycmd).readlines())
    return result

def send_times(mycmd):
    times = 0
    outcome = sendCmd(mycmd)
    while times < 3:
        if not outcome or outcome == "Empty":
            times += 1
            time.sleep(3)
        else:
            time.sleep(3)
            return outcome
    return outcome

def check_devices_mode():
    times = 0
    while times < 2:
        check_mode_cmd = "%s LD" % loader_tool_path
        g = sendCmd(check_mode_cmd)
        print(g)
        if "Mode=Loader" in g:
            print("3568 board has entered the Loader mode successfully!")
            return True
        else:
            print("kill the process")
            os.system("hdc_std kill")
            time.sleep(5)
            print("start the process")
            os.system("hdc_std start")
            time.sleep(5)
            os.system("hdc_std shell reboot loader")
            time.sleep(10)
            times += 1
    print("Failed to enter the loader mode!")
    return False

def checkProcess():
    ren_cmd_1 = "hdc_std shell pidof render_service"
    u = sendCmd(ren_cmd_1)
    print("pid of render_service: %s" % u)
    if "[Fail]ExecuteCommand need connect-key" in u:
        print("device disconnection!")
        return False
    time.sleep(5)
    ren_cmd_2 = "hdc_std shell pidof render_service"
    v = sendCmd(ren_cmd_2)
    print("pid of render_service: %s" % v)
    time.sleep(5)
    laun_cmd_1 = "hdc_std shell pidof com.ohos.launcher"
    w = sendCmd(laun_cmd_1)
    print("pid of launcher: %s" % w)
    time.sleep(5)
    laun_cmd_2 = "hdc_std shell pidof com.ohos.launcher"
    x = sendCmd(laun_cmd_2)
    print("pid of launcher: %s" % x)
    if re.search('\d+', u) and re.search('\d+', w) and u == v and w == x:
        print("pids are same!")
        return True
    else:
        print("pids are not same!")
        return False

def flash_version():
    partList = ["boot_linux", "system", "vendor", "userdata", "resource", "ramdisk", "chipset", "sys-prod", "chip-prod"]
    for i in partList:
        if not os.path.exists("%s/%s.img" % (local_image_path, i)):
            print("%s.img is not exist, ignore" % i)
            continue
        loadcmd = "%s DI -%s %s/%s.img" % (loader_tool_path, i, local_image_path, i)
        p = sendCmd(loadcmd)
        print(p)
        # time.sleep(5)
        if "Download image ok" not in p:
            print("Failed to download the %s.img!" % i)
            return False
        else:
            print("The %s.img downloaded successfully!" % i)
    return True

def hdc_kill():
    print("kill the process")
    os.system("hdc_std kill")
    print("start the process")
    os.system("hdc_std start")

def rk3568_upgrade(imgPath, toolPath):
    '''
    #======================================================
    #   @Method:        upgrade(self)
    #   @Precondition:  none
    #   @Func:          升级相关业务逻辑
    #   @PostStatus:    none
    #   @eg:            upgrade()
    #   @return:        True or Flase
    #=====================================================
    '''
    global local_image_path
    global loader_tool_path
    local_image_path = imgPath
    loader_tool_path = toolPath

    if not check_devices_mode():
        check_devices_cmd = "hdc_std list targets"
        f = send_times(check_devices_cmd)
        print(f)
        if not f or "Empty" in f:
            print("No devices found,please check the device.")
            return False
        else:
            print("3568 board is connected.")
            return check_devices_mode()
    else:
        upgrde_loader_cmd = "%s UL %s/MiniLoaderAll.bin -noreset" % (loader_tool_path, local_image_path)
        h = sendCmd(upgrde_loader_cmd)
        print(h)
        if "Upgrade loader ok" not in h:
            print("Download MiniLoaderAll.bin Fail!")
            return False
        else:
            print("Download MiniLoaderAll.bin Success!")
            # time.sleep(3)
            write_gpt_cmd = "%s DI -p %s/parameter.txt" % (loader_tool_path, local_image_path)
            j = sendCmd(write_gpt_cmd)
            print(j)
            if "Write gpt ok" not in j:
                print("Failed to execute the parameter.txt")
                return False
            else:
                print("Successfully executed parameter.txt.")
                # time.sleep(5)
                download_uboot_cmd = "%s DI -uboot %s/uboot.img %s/parameter.txt" % (
                    loader_tool_path, local_image_path, local_image_path)
                k = sendCmd(download_uboot_cmd)
                print(k)
                if "Download image ok" not in k:
                    print("Failed to download the uboot.image!")
                    return False
                else:
                    print("The uboot.image downloaded successfully!")
                    # time.sleep(5)
                    if not flash_version():
                        return False
                    reboot_devices_cmd = "%s RD" % loader_tool_path
                    reboot_result = sendCmd(reboot_devices_cmd)
                    print(reboot_result)
                    time.sleep(15)
                    hdc_kill()
                    power_cmd = "hdc_std shell \"power-shell setmode 602\""
                    hilog_cmd = "hdc_std shell \"hilog -w start -l 400000000 -m none\""
                    power_result = sendCmd(power_cmd)
                    print(power_result)
                    number = 0
                    while "Set Mode Success" not in power_result and number < 60:
                        time.sleep(2)
                        power_result = sendCmd(power_cmd)
                        print(power_result)
                        number += 1
                    hilog_result = sendCmd(hilog_cmd)
                    print(hilog_result)
                    time.sleep(10)
                    if "Reset Device OK" not in reboot_result:
                        print("Failed to reboot the board!")
                        return False
                    else:
                        print("Reboot successfully!")
                        print("******DownLoader img Finished and upload successfully******")
                        print("Start to check processes of the board!")
                        os.system("hdc_std kill")
                        time.sleep(5)
                        os.system("hdc_std start")
                        time.sleep(5)
                        times = 0
                        while times < 3:
                            if checkProcess():
                                time.sleep(5)
                                check_process_cmd = "hdc_std shell ps -elf"
                                t = sendCmd(check_process_cmd)
                                print(t)
                                list_ret = re.findall("render_service|com.ohos.launcher", t)
                                print(list_ret)
                                return True
                            else:
                                print("Try again!")
                                times += 1
                        print("Check the screen of the board!")
                        return False

if __name__ == "__main__":
    vesion_dir = sys.argv[1]
    tar_exe_path = sys.argv[2]
    burn_tool_path = sys.argv[3]

    # 获取当前目录的目录名
    name = os.path.basename(vesion_dir)
    print(name)
    # tar
    image_path_du200 = tar_gz(vesion_dir, tar_exe_path)
    print(image_path_du200)

    # burn
    rk3568_upgrade(image_path_du200,burn_tool_path)
    time.sleep(10)
    sys.exit()
