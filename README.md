# 1. **MyOpenHarmony项目**

- [1. **MyOpenHarmony项目**](#1-myopenharmony项目)
  - [1.1. MyOpenHarmony介绍](#11-myopenharmony介绍)
  - [1.2. 项目目录树介绍](#12-项目目录树介绍)
  - [1.3. 快速查看目录](#13-快速查看目录)
    - [1.3.1. 环境配置](#131-环境配置)
      - [1.3.1.1. ubuntu环境搭建](#1311-ubuntu环境搭建)
      - [1.3.1.2. 代码下载编译上库](#1312-代码下载编译上库)
    - [1.3.2. 方案设计](#132-方案设计)
    - [1.3.3. 开发指导](#133-开发指导)
      - [1.3.3.1. 测试开发指导文档](#1331-测试开发指导文档)
      - [1.3.3.2. 加速度](#1332-加速度)
      - [1.3.3.3. 环境光](#1333-环境光)
      - [1.3.3.4. 地磁计](#1334-地磁计)
      - [1.3.3.5. 气压计](#1335-气压计)
      - [1.3.3.6. 接近光](#1336-接近光)
      - [1.3.3.7. 霍尔](#1337-霍尔)
    - [1.3.4. 效率工具](#134-效率工具)
      - [1.3.4.1. 测试验证环境](#1341-测试验证环境)
      - [1.3.4.2. 公版驱动依赖工具](#1342-公版驱动依赖工具)
      - [1.3.4.3. astah](#1343-astah)
      - [1.3.4.4. hdi\_export](#1344-hdi_export)
      - [1.3.4.5. 代码工具](#1345-代码工具)
      - [1.3.4.6. 常用工具命令](#1346-常用工具命令)
      - [1.3.4.7. 开发工具](#1347-开发工具)
    - [1.3.5. 总结分享](#135-总结分享)


## 1.1. MyOpenHarmony介绍
本项目用于Kevin OpenHarmony项目的开发和交流，主要介绍OpenHarmony开发所需要的环境搭建、驱动开发指导、模块方案设计、开发过程中的经验分享等。

## 1.2. 项目目录树介绍

```bash
--code    // demo示例代码
--doc     // 相关开发指导文档，测试文档，设计文档
--test    // 测试用例及相关工具
--tools   // 设计，开发，效率提升工具
```

## 1.3. 快速查看目录
### 1.3.1. 环境配置
#### 1.3.1.1. [ubuntu环境搭建](https://gitee.com/Kevin-Lau/my-open-harmony/wikis/openharmonyL1%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA?sort_id=3652948)

#### 1.3.1.2. [代码下载编译上库](https://gitee.com/Kevin-Lau/my-open-harmony/wikis/%E6%8F%90%E4%BA%A4%E4%BB%A3%E7%A0%81%E6%93%8D%E4%BD%9C?sort_id=3658086)
### 1.3.2. 方案设计

### 1.3.3. 开发指导
#### 1.3.3.1. [测试开发指导文档](https://gitee.com/Kevin-Lau/my-open-harmony/blob/master/doc/readme.md)
#### 1.3.3.2. [加速度](https://gitee.com/Kevin-Lau/my-open-harmony/blob/master/doc/sensor/README.md)
#### 1.3.3.3. [环境光](https://gitee.com/Kevin-Lau/my-open-harmony/blob/master/doc/sensor/als/%E7%8E%AF%E5%A2%83%E5%85%89%E9%A9%B1%E5%8A%A8%E5%BC%80%E5%8F%91.md)
#### 1.3.3.4. [地磁计](https://gitee.com/Kevin-Lau/my-open-harmony/blob/master/doc/sensor/mag/%E5%9C%B0%E7%A3%81%E8%AE%A1%E9%A9%B1%E5%8A%A8%E5%BC%80%E5%8F%91.md)
#### 1.3.3.5. [气压计](https://gitee.com/Kevin-Lau/my-open-harmony/blob/master/doc/sensor/bar/%E6%B0%94%E5%8E%8B%E8%AE%A1%E9%A9%B1%E5%8A%A8%E5%BC%80%E5%8F%91.md)
#### 1.3.3.6. [接近光](https://gitee.com/Kevin-Lau/my-open-harmony/blob/master/doc/sensor/proximity/%E6%8E%A5%E8%BF%91%E5%85%89%E9%A9%B1%E5%8A%A8%E5%BC%80%E5%8F%91.md)
#### 1.3.3.7. [霍尔](https://gitee.com/Kevin-Lau/my-open-harmony/blob/master/doc/sensor/hall/%E9%9C%8D%E5%B0%94%E9%A9%B1%E5%8A%A8%E5%BC%80%E5%8F%91%20.md)

### 1.3.4. 效率工具
#### 1.3.4.1. [测试验证环境](https://gitee.com/Kevin-Lau/my-open-harmony/wikis/nfs%E6%8C%82%E8%BD%BD%E5%91%BD%E4%BB%A4?sort_id=3658750)
#### 1.3.4.2. [公版驱动依赖工具](https://gitee.com/Kevin-Lau/my-open-harmony/wikis/%E5%B7%A5%E5%85%B7%E4%B8%8B%E8%BD%BD%E6%96%B9%E5%BC%8F?sort_id=3658325)
#### 1.3.4.3. [astah](tools/astah/)
#### 1.3.4.4. [hdi_export](tools/hdi_export_tool/)
#### 1.3.4.5. [代码工具](tools/code_tools/)
#### 1.3.4.6. [常用工具命令](tools/cmd_tools/)
#### 1.3.4.7. [开发工具](tools/develop_tools/)

### 1.3.5. 总结分享


更多介绍请到本项目Wiki里参观~