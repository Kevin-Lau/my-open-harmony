1. 使用```"#"```作为标题级别即可，```"#"```符号越多，标题越小
2. 无序标题 前面是点，```"-"、"+" "*"```即可
3. 添加链接: ```[显示文本](链接URL)```
4. 完成第一个标题及第一个目录匹配即可，后续再添加标题会自动更新目录完成第一个标题及第一个目录匹配即可，后续再添加标题会自动更新目录，命令：Markdown All in One:Create Table of Contents
5. 添加标题：Markdown All in One:Add/Update Section numbers
6. 更新标题：Markdown All in One:Update Table of Contents
7. 添加行内式图片：```![图片名](图片路径及图片名)```
8. 添加链接式图片：链接式格式：! + [替代图片的文字，可空] + [参考id]
[参考id]: 路径 URL
9. 加粗：```**待加粗的文字**```
10. 倾斜：``` *倾斜文字*```
11. 倾斜加粗：```***倾斜加粗***```
12. 高亮：```==高亮的文字==```
13. 引用文本： 使用```"> 文本"``` 支持嵌套
14. 删除文本：```~~删除文本~~```
15. 缩进：```&emsp;```缩进1个字； ```&nbsp;```常规空格宽度；```&ensp;```一个字母宽度
16. 换行：```"<br>"```
17. 目录命令：``` @[TOC](标题)```
18. 注释： *[dfdfsfd]
19. 行代码：`前后包裹 
20. 块代码：```前后包裹
21. 分割线：* - _ 三中线效果不同，明显，不明显，一般
22. 复选框：列表符号+*-后面加上[]或者[x]代表选中和未选中 例如：- [X] 学习。或者不用列表符，直接 [ ]
23. 反斜杠 \ 反斜杠+ \ ` * _ {} [] () # + - ! .
24. 添加注释[^注释]  换行后[^注释]： 
25. 文字居中: ``` <center>这是要居中的文本内容</center>
或者<div  align="center">这是要居中的文本内容</div> ```
26. 图片居中：``` <img src = "http://qiniu.daidaixiang.xyz/image-20201110155239356.png" align="center">
或者<div  align="center">
<img src="http://qiniu.daidaixiang.xyz/image-20201110155239356.png"/> </div> ```
27. 图片缩放：``` <img src="http://qiniu.daidaixiang.xyz/image-20201110155239356.png" align="right" style="zoom:50%"/> 或者<div align="center"> <img src="http://qiniu.daidaixiang.xyz/image-20201110155239356.png" width = 20% height = 20%/> </div> 或者<div  align="center"> <img src="http://qiniu.daidaixiang.xyz/image-20201110155239356.png" width = "100" height = "100"/> </div>```









